#SSPopupHeaderView

SSPopupHeaderView is a view class that is designed to make your top table view cell appear like it's coming down from the UINavigationBar, similar to a UIPopupView.

##Usage

```objc
- (void)viewDidLoad {

    [super viewWillAppear:animated];
    
    UIColor *navigationColor = [UIColor colorWithRed:254.0f / 255.0f green:248.0f / 255.0f blue:231.0f / 255.0f alpha:1.0];

    //Prevent the shadow of our UINavigationBar from drawing
    [self.navigationController.navigationBar setBackgroundImage:[UIImage new] forBarMetrics:UIBarMetricsDefault];
    self.navigationController.navigationBar.shadowImage = [UIImage new];
    self.navigationController.navigationBar.barTintColor = navigationColor;

    //configure our headerview
    SSPopupHeaderView *headerView = [SSPopupHeaderView new];
    headerView.bottomColor = self.tableView.backgroundColor;
    headerView.topColor = self.navigationController.navigationBar.barTintColor;
    headerView.borderColor = self.tableView.separatorColor;
    headerView.barButtonItem = self.barButtonItem;

    self.tableView.tableHeaderView = headerView;

}
```

##Screenshot

![Example Screenshot](https://bitbucket.org/jamesvandyne/sspopupheaderview/raw/master/example.png)