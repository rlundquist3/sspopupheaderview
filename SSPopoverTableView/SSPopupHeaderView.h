//
//  SSPopupHeaderView.h
//  SSPopoverTableView
//
//  Created by James Van Dyne on 12/20/13.
//  Copyright (c) 2013 Sugoi Software, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface SSPopupHeaderView : UIView

@property (readwrite, assign) NSInteger triangleHeight;

/** Color of the line  that draws the triangle. Defaults to the tableview's separator color */
@property UIColor *borderColor;

/** Color to fill above the triangle. Should match the UINavigationBar. Defaults to white. */
@property UIColor *topColor;

/** Color to fill below the line. Should match your top most UITableviewCell. Defaults to white. */
@property UIColor *bottomColor;

/** The table will appear to come from the center of this button. */
@property UIBarButtonItem *barButtonItem;

@end
