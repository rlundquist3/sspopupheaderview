//
//  SSPopupHeaderView.m
//  SSPopoverTableView
//
//  Created by James Van Dyne on 12/20/13.
//  Copyright (c) 2013 Sugoi Software, LLC. All rights reserved.
//

#import "SSPopupHeaderView.h"

@implementation SSPopupHeaderView

@synthesize triangleHeight;

- (id)init {
    self = [super init];
    if (self) {
        
        UIScreen *screen = [UIScreen mainScreen];
        
        self.frame = CGRectMake(0, 0, screen.bounds.size.width, 20.0f);
        
        // Initialization code
        self.triangleHeight = 30;
        self.borderColor = [UIColor colorWithRed:224/255.0 green:224/255.0 blue:224/255.0 alpha:1.0];
        self.topColor = [UIColor whiteColor];
        self.bottomColor = [UIColor whiteColor];
    }
    return self;
    
}


- (CGRect)frameForBarButtonItem:(UIBarButtonItem *)buttonItem
{
    UIView *view = [buttonItem valueForKey:@"view"];
    return  view ? view.frame : CGRectZero;
}


- (void)drawRect:(CGRect)rect
{
    
    //draw a straight line across the top of the table / bottom of our view
    CGPoint startPoint = CGPointMake(0, self.triangleHeight / 2);
    
    //let's just make it 3/4 of the screen before we start our triangle
    CGPoint startTriangle = CGPointMake(rect.size.width * 0.75f, startPoint.y);
    
    //center on our bar button item if they exist
    if(self.barButtonItem) {
        CGRect barButtonRect = [self frameForBarButtonItem:self.barButtonItem];
        startTriangle = CGPointMake(barButtonRect.origin.x + ((barButtonRect.size.width / 2) - self.triangleHeight / 2), startPoint.y);
    }
    
    CGPoint topTriangle = CGPointMake(startTriangle.x + (self.triangleHeight / 2),
                                      startTriangle.y - (self.triangleHeight / 2 ));
    CGPoint endTriangle = CGPointMake(topTriangle.x + (self.triangleHeight / 2), startTriangle.y);
    CGPoint endLine = CGPointMake(rect.size.width, startTriangle.y);
    
    
    UIBezierPath *headerLine = [UIBezierPath bezierPath];
    [headerLine moveToPoint:startPoint];
    [headerLine addLineToPoint:startTriangle];
    [headerLine addLineToPoint:topTriangle];
    [headerLine addLineToPoint:endTriangle];
    [headerLine addLineToPoint:endLine];
    headerLine.lineWidth = 0.5f;
    
    
    //give the appearance of extending our navigation area
    UIBezierPath *topColorPath = [UIBezierPath bezierPath];
    
    CGPoint topRight = CGPointMake(rect.size.width, 0);
    CGPoint topLeft = CGPointMake(0,0);
    
    [topColorPath moveToPoint:topLeft];
    [topColorPath addLineToPoint:startPoint];
    [topColorPath addLineToPoint:startTriangle];
    [topColorPath addLineToPoint:topTriangle];
    [topColorPath addLineToPoint:endTriangle];
    [topColorPath addLineToPoint:endLine];
    [topColorPath addLineToPoint:topRight];
    [topColorPath closePath];

    [self.topColor set];
    [topColorPath fill];
    
    //fill in our bottom color to extend our tableview
    
    UIBezierPath *bottomColorPath = [UIBezierPath bezierPath];
    CGPoint bottomRight = CGPointMake(rect.size.width, rect.size.height);
    CGPoint bottomLeft = CGPointMake(0,rect.size.height);
    
    [bottomColorPath moveToPoint:bottomLeft];
    [bottomColorPath addLineToPoint:startPoint];
    [bottomColorPath addLineToPoint:startTriangle];
    [bottomColorPath addLineToPoint:topTriangle];
    [bottomColorPath addLineToPoint:endTriangle];
    [bottomColorPath addLineToPoint:endLine];
    [bottomColorPath addLineToPoint:bottomRight];
    [bottomColorPath closePath];
    [self.bottomColor set];
    [bottomColorPath fill];
    
    //draw our line
    [self.borderColor set];
    [headerLine stroke];
 
}


@end
