//
//  SSViewController.m
//  SSPopoverTableView
//
//  Created by James Van Dyne on 12/20/13.
//  Copyright (c) 2013 Sugoi Software, LLC. All rights reserved.
//

#import "SSViewController.h"
#import "SSPopupHeaderView.h"

@interface SSViewController ()

@end

@implementation SSViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    UIColor *navigationColor = [UIColor colorWithRed:254.0f / 255.0f green:248.0f / 255.0f blue:231.0f / 255.0f alpha:1.0];
	// Do any additional setup after loading the view, typically from a nib.
    SSPopupHeaderView *headerView = [SSPopupHeaderView new];
    headerView.bottomColor = self.tableView.backgroundColor;
    headerView.topColor = navigationColor;
    headerView.borderColor = self.tableView.separatorColor;
    headerView.barButtonItem = self.barButtonItem;
    self.tableView.tableHeaderView = headerView;
    
    //don't draw our border for the navigation bar
    [self.navigationController.navigationBar setBackgroundImage:[UIImage new] forBarMetrics:UIBarMetricsDefault];
    self.navigationController.navigationBar.shadowImage = [UIImage new];
    self.navigationController.navigationBar.barTintColor = navigationColor;
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
