//
//  SSAppDelegate.h
//  SSPopoverTableView
//
//  Created by James Van Dyne on 12/20/13.
//  Copyright (c) 2013 Sugoi Software, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SSAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
